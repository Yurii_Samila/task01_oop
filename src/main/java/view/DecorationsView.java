package view;
import models.ChristmasDecoration;
import java.util.ArrayList;
import java.util.Comparator;
public class DecorationsView {
public void startMenu() {
  System.out.println("Welcome to Christmas Fair!!!\n"
      + "At our Fair you can buy any decorations in these categories:\n"
      + "1) Decorations for room\n"
      + "2) Decorations for house\n"
      + "3) Decorations for Christmas tree\n"
      + "4) Decorations for outside trees\n"
      + "Please, choose decorations you want to see."
      + " After Your choice, please, press '0'");
}

  public void continueMenu(){
    System.out.println("1) Main menu: press any key\n"
        + "2) Exit: press '0'");
  }

  public void show(ArrayList<ChristmasDecoration> list){
    list.stream()
        .sorted((Comparator.comparing(o -> o.getClass().toString())))
        .forEach(System.out::println);
  }
  }

