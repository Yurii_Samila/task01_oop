package controllers;

import models.ChristmasDecoration;
import models.DecorationModel;
import view.DecorationsView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MainController {
  private DecorationModel model = new DecorationModel();
  private DecorationsView view = new DecorationsView();
  private int choice = -1;
  private Scanner sc = new Scanner(System.in);
  private HashSet<String> buyerChoice() {
    HashSet<String> chosenTypes = new HashSet<>();
    while (choice != 0) {
      choice = sc.nextInt();
      switch (choice) {
        case 1:
          chosenTypes.add("class models.RoomDecoration");
          break;
        case 2:
          chosenTypes.add("class models.HouseDecoration");
          break;
        case 3:
          chosenTypes.add("class models.ChristmasTreeDecoration");
          break;
        case 4:
          chosenTypes.add("class models.OutsideTreeDecoration");
          break;
          default:
            System.out.println("Please,make your choise");
            break;
      }
    }
    return chosenTypes;
  }
  private ArrayList<ChristmasDecoration> showSortedDecorations() {
    HashSet<String> buyerChoiceSet = buyerChoice();
    ArrayList<ChristmasDecoration> decorations = model.getDecorations();
    ArrayList<ChristmasDecoration> decorationsFiltered = new ArrayList<>();
    Iterator iterator = buyerChoiceSet.iterator();
    while (iterator.hasNext()) {
      String next = (String) iterator.next();
      List<ChristmasDecoration> filtered = decorations.stream()
          .filter(o -> o.getClass().toString().equals(next))
          .collect(Collectors.toList());
      decorationsFiltered.addAll(filtered);
    }
    return decorationsFiltered;
  }
  public void start() {
    while (choice != 0) {
      view.startMenu();
      ArrayList<ChristmasDecoration> christmasDecorations = showSortedDecorations();
      view.show(christmasDecorations);
      view.continueMenu();
      choice = sc.nextInt();
    }
  }
}
