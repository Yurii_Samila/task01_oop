package models;
import java.util.Objects;
public class RoomDecoration extends ChristmasDecoration {
  private String typeOfDecor;

  public RoomDecoration(int id, int price, String color, String typeOfDecor) {
    super(id, price, color);
    this.typeOfDecor = typeOfDecor;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    RoomDecoration that = (RoomDecoration) o;
    return Objects.equals(typeOfDecor, that.typeOfDecor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), typeOfDecor);
  }

  @Override
  public String toString() {
    return "RoomDecoration{"
        + "id="
        + getId()
        + ", price= "
        + getPrice()
        + ", color='"
        + getColor()
        + "', typeOfDecor='"
        + typeOfDecor
        + '\''
        + '}';
  }
}
