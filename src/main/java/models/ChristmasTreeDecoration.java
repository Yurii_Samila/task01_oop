package models;
import java.util.Objects;
public class ChristmasTreeDecoration extends ChristmasDecoration {
  private int weight;

  public ChristmasTreeDecoration(int id, int price, String color, int weight) {
    super(id, price, color);
    this.weight = weight;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    ChristmasTreeDecoration that = (ChristmasTreeDecoration) o;
    return weight == that.weight;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), weight);
  }

  @Override
  public String toString() {
    return "ChristmasTreeDecoration{"
        + "id="
        + getId()
        + ", price="
        + getPrice()
        + ", color='"
        + getColor()
        + "', weight="
        + weight
        + '}';
  }
}
