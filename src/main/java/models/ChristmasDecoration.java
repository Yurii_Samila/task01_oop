package models;
import java.util.Objects;
public abstract class ChristmasDecoration {
  private int id;
  private int price;
  private String color;
  ChristmasDecoration(int id, int price, String color) {
    this.id = id;
    this.price = price;
    this.color = color;
  }
  int getId() {
    return id;
  }

  int getPrice() {
    return price;
  }

  String getColor() {
    return color;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChristmasDecoration that = (ChristmasDecoration) o;
    return id == that.id
        && price == that.price
        && Objects.equals(color, that.color);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, color);
  }

  @Override
  public String toString() {
    return "ChristmasDecoration{"
        + "id="
        + id
        + ", price="
        + price
        + ", color='"
        + color
        + '\''
        + '}';
  }
}
