package models;
import java.util.Objects;
public class OutsideTreeDecoration extends ChristmasDecoration {
  private int size;

  OutsideTreeDecoration(int id, int price, String color, int size) {
    super(id, price, color);
    this.size = size;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    OutsideTreeDecoration that = (OutsideTreeDecoration) o;
    return size == that.size;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), size);
  }

  @Override
  public String toString() {
    return "OutsideTreeDecoration{"
        + "id="
        + getId()
        + ", price="
        + getPrice()
        + ", color='"
        + getColor()
        + "', size="
        + size
        + '}';
  }
}
