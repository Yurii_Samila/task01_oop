package models;
import java.util.ArrayList;
public class DecorationModel {
  public ArrayList<ChristmasDecoration> getDecorations(){
    ArrayList<ChristmasDecoration> decorations = new ArrayList<>();
    decorations.add(new ChristmasTreeDecoration(1, 50, "Blue", 500));
    decorations.add(new RoomDecoration(2, 30, "White", "Lights"));
    decorations.add(new OutsideTreeDecoration(3, 40, "Black", 5));
    decorations.add(new HouseDecoration(4, 10, "Grey", true));
    decorations.add(new ChristmasTreeDecoration(5, 15, "Red", 500));
    decorations.add(new ChristmasTreeDecoration(6, 70, "Yellow", 450));
    decorations.add(new RoomDecoration(7, 45, "Pink", "Ball"));
    decorations.add(new OutsideTreeDecoration(8, 55, "Black", 3));
    decorations.add(new HouseDecoration(9, 25, "Orange", false));
    decorations.add(new ChristmasTreeDecoration(10, 65, "Brown", 750));
    return decorations;
  }
}
