package models;
import java.util.Objects;
public class HouseDecoration extends ChristmasDecoration {
  private boolean lights;

  HouseDecoration(int id, int price, String color, boolean lights) {
    super(id, price, color);
    this.lights = lights;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    HouseDecoration that = (HouseDecoration) o;
    return lights == that.lights;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), lights);
  }

  @Override
  public String toString() {
    return "HouseDecoration{"
        + "id="
        + getId()
        + ", price="
        + getPrice()
        + ", color='"
        + getColor()
        + "', lights="
        + lights
        + '}';
  }
}
